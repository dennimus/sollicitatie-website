window.addEventListener("load",init);

// array met de namen van de foto's gebruikt in de game
var shot = new Audio("./sound/shot.wav");
var applaus = new Audio("./sound/applaus.wav");
var twins = ['bridges','craig','eastwood','ford','matthew','portman','pratt','saddles','wilson','harrelson','jones','brolin'];
var finder = Math.floor(Math.random()*11);

// functie om de volgorde van een array te randomizen

function randomizeArray(){
    var i = this.length, j, temp;
    if (i === 0) return false;
    while(--i){
        j = Math.floor( Math.random() * (i+1));
        temp = this[i];
        this[i] = this[j];
        this[j] = temp;
    }
}
// hierdoor kun je de randomizeArray functie makkelijk gebruiken op nieuwe objecten

Array.prototype.randomize = randomizeArray;

var pictureSelect = [0,1,2,3,4,5,6,7,8,9,10,11];

//randomize volgorde van nummers voor de foto's

pictureSelect.randomize();

function init(){
    // met deze loop wordt het grid gevuld met random plaatjes
    for (var b = 1; b < 13; b++){
        var pictureNumber = pictureSelect.pop();
        document.getElementById("picture" + b).src="img/memory/" + twins[pictureNumber] + ".jpg";
        document.getElementById("picture" + b).setAttribute('onclick', 'send(' +pictureNumber+ ');')
    }

    // hier wordt het gezochte plaatje gegenereerd
    finder = Math.floor(Math.random()*11);
    document.getElementById("finder").src="img/memory/" +  twins[finder] + ".jpg";
}
// checkt of je het goede plaatje hebt gekozen
function send(imageclicked){
    console.log(imageclicked);
    console.log(finder);
    if (imageclicked == finder){
        applaus.play();
        alert("Great Job!");
        location.reload();
    }
    else{
        shot.play();
        alert("That aint no family o'mine");
        shot = new Audio("./sound/shot.wav");
    }
}

